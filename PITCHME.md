---?color=#ffffff
# Basic page 

An application architecture for React

- Item1
- Item2
- Item3

---
@title[Statement]

### Let your
## @css[shoutout](Code)
#### Do The Talking


---
@title[Page down]

### Example with page down



@fa[arrow-down]

+++
@title[Test]

#### More details

<br>

The *same syntax* you use to create project   
**READMEs** and **Wikis** for your Git repos.

+++
@title[One more]

#### Even More details


---?color=#ffffff
@title[Colored page]

# @size[3.5em](NO!)

---
@title[Use CSS defined title]

@css[title-top-right](Top-right-title)


---?image=assets/images/test.jpg&size=65% 100%&position=left
@title[Figure from png]

### Awesome
# PHOTO
#### IN TUSCANY

---
@title[Figure from web]

![Flux Explained](https://facebook.github.io/flux/img/flux-simple-f8-diagram-explained-1300w.png)


---?color=black
@snap[north]
# @color[red](Layout images side-by-side)
@snapend

@div[left-50]
<br>
![FIG1](assets/images/test.jpg)
@size[0.5em]@color[white](Fig1)

@divend

@div[right-50]
<br>
![FIG2](assets/images/test.jpg)
@divend


---
@title[Embed Video]
## Video Slides
## [ Inline ]
<span style="font-size:0.6em; color:gray">Press Down key for examples.</span> |
<span style="font-size:0.6em; color:gray">See [GitPitch Wiki](https://github.com/gitpitch/gitpitch/wiki/Video-Slides) for details.</span>

@fa[arrow-down]

+++
@title[MP4]

![](assets/images/Video.gif)

Note: ![MP4 Video](assets/images/Video.mp4) does not work unless Pro. Add this at the end of link to see notes in main screen ?n=true


